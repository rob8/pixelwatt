<?php 
/* 
Template Name: Case Study
*/ 
?>

<?php get_header(); ?>
  <?php 
    $key = get_post_meta( $post->ID, '_casestudy_key', true );
    echo ( ! empty( $key ) ? pixelwatt_build_cs( $post->ID, $key ) : '' );
  ?>
<?php get_footer(); ?>