$('.owl-carousel-website').owlCarousel({
    stagePadding: 200,
    loop:true,
    margin:10,
    nav:false,
    items:1,
    lazyLoad: true,
    nav:false,
  responsive:{
        0:{
            items:1,
            stagePadding: 60
        },
        600:{
            items:1,
            stagePadding: 100
        },
        1000:{
            items:1,
            stagePadding: 200
        },
        1200:{
            items:1,
            stagePadding: 250
        },
        1400:{
            items:1,
            stagePadding: 300
        },
        1600:{
            items:1,
            stagePadding: 350
        },
        1800:{
            items:1,
            stagePadding: 400
        }
    }
});

$('body.home .owl-carousel').owlCarousel({
                    loop:true,
                    margin:0,
                    nav:false,
                    items:1,
                    dots: false,
                    autoplay: true,
                    stagePadding: 0,
                    smartSpeed: 1000
                  });

owl_website = $('.owl-carousel-website').owlCarousel();
$(".prev-website").click(function () {
    owl_website.trigger('prev.owl.carousel');
});

$(".next-website").click(function () {
    owl_website.trigger('next.owl.carousel');
});

function openNav() {
    document.getElementById("navOverlay").style.height = "100%";
}
function closeNav() {
    document.getElementById("navOverlay").style.height = "0%";
}