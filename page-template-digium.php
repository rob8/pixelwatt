<?php 
/* 
Template Name: Digium
*/ 
?>

<?php get_header(); ?>
    <header class='header-casestudy jarallax' id='header-digium' data-jarallax data-speed="0.6" style="background-image: url(<?php bloginfo('template_directory') ?>/images/digium-header.jpg);">
      <?php echo pixelwatt_build_nav_container(); ?>
      <div class='header-content-container'>
        <div class='container header-content'>
          <div class='row'>
            <div class='col-12'>
              <div class='row justify-content-center'>
                <div class='col-12 col-md-10 text-center'>
                  <h1>
                    Bringing Cross-Platform, Cross-Brand Consistency to Digium's Web Properties
                  </h1>
                  <p class='subtitle'>
                    Client: Digium
                  </p>
                  <p>
                    Digium is an open communications company that provides it's clients with affordable, cutting edge phone systems. Digium also maintains the Asterisk open source project, which powers many of it's projects. In mid 2016, their web properties were numerous, spread across many different platforms, powered by many different frameworks. As a full-time employee, I embarked on a project to bring all of these sites together under a unified style guide by creating a universal framework that could be used across any Digium or Asterisk site, regardless of brand or platform.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <hr>
    <div class='casestudy-content'>
      <div class='casestudy-services text-center'>
        <div class='container'>
          <div class='row'>
            <div class='col-12'>
              <h2>
                Services
              </h2>
            </div>
          </div>
          <div class='row'>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdesign.png'>
                </div>
              </div>
              <h4>
                Website Design
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdev.png'>
                </div>
              </div>
              <h4>
                Wordpress &amp; Drupal Development
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-security.png'>
                </div>
              </div>
              <h4>
                Security &amp; Backups
              </h4>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class='casestudy-website' id='cityofhsv-website'>
        <div class='container'>
          <div class='row justify-content-center'>
            <div class='col-12 col-md-10 text-center'>
              <h2>
                Poundkey: A Universal Framework
              </h2>
              <p>
                Ultimately, I created Poundkey, a framework built to fit any site while still providing enough flexibility to work for both Digium and Asterisk brands. Below are excerpts from the <a href="http://www.asterisk.org/">Asterisk</a>, <a href="http://www.asterisk.org/community/astricon-user-conference">AstriCon</a>, and <a href="https://digium.com">Digium</a> websites that utilize the framework.
              </p>
            </div>
          </div>
        </div>
        <div class='owl-carousel owl-carousel-website'>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/asterisk-web-01.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/asterisk-web-02.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/astricon-web-01.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/astricon-web-02.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/astricon-web-03.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/digium-web-01.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/digium-web-02.png' width='1282'>
          </div>
        </div>
      </div>
      <hr>
    </div>
    <div class='casestudy-contact'>
      <div class='container'>
        <div class='row justify-content-center'>
          <div class='col-12 col-sm-10 text-center'>
            <h2>
              PixelWatt helps Digium efficiently expand it's web presence.
              <strong>
                Let’s talk about what we could do for your business.
              </strong>
            </h2>
            <p>
              Fill out the form below to get the conversation started.
            </p>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
            	<?php the_content(); ?>
            
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>