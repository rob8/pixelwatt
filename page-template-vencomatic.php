<?php 
/* 
Template Name: Vencomatic
*/ 
?>

<?php get_header(); ?>
    <header class='header-casestudy jarallax' id='header-vencomatic' data-jarallax data-speed="0.6" style="background-image: url(<?php bloginfo('template_directory') ?>/images/vencomatic-header.jpg);">
      <?php echo pixelwatt_build_nav_container(); ?>
      <div class='header-content-container'>
        <div class='container header-content'>
          <div class='row'>
            <div class='col-12'>
              <div class='row justify-content-center'>
                <div class='col-12 col-md-10 text-center'>
                  <h1>
                    Rebranding North America's Premier Provider of Innovative Poultry Equipment
                  </h1>
                  <p class='subtitle'>
                    Client: Vencomatic North America
                  </p>
                  <p>
                    Vencomatic North America is a leading provider of innovative equipment for the poultry industry in the United States and Canada. In early 2016, Vencomatic embarked on a full-scale rebranding effort for both their main <a href="http://vencomatic.ca/" target="_blank">Vencomatic North America</a> brand and smaller <a href="http://davian.us/" target="_blank">Davian</a> brand. Working with Story Communications for copywriting and content strategy, PixelWatt designed a new visual identity, set of websites, and suite of promotional materials for both brands, creating a shared, cohesive style for both properties. 
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <hr>
    <div class='casestudy-content'>
      <div class='casestudy-services text-center'>
        <div class='container'>
          <div class='row'>
            <div class='col-12'>
              <h2>
                Services
              </h2>
            </div>
          </div>
          <div class='row'>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdesign.png'>
                </div>
              </div>
              <h4>
                Website Design
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdev.png'>
                </div>
              </div>
              <h4>
                Wordpress Development
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-security.png'>
                </div>
              </div>
              <h4>
                Security &amp; Backups
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-maintenance.png'>
                </div>
              </div>
              <h4>
                Website Maintenance
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-email.png'>
                </div>
              </div>
              <h4>
                Responsive Email
              </h4>
            </div>
            <div class='col-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-promo-materials.png'>
                </div>
              </div>
              <h4>
                Promotional Materials
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div class='casestudy-image jarallax' id='vencomatic-tradeshow' data-jarallax data-speed="0.8" style="background-image: url(<?php bloginfo('template_directory') ?>/images/vencomatic-tradeshow.jpg);">
        <div class='casestudy-caption'>
          Showroom Product Posters
        </div>
      </div>
      <hr>
      <div class='casestudy-website' id='vencomatic-website'>
        <div class='container'>
          <div class='row justify-content-center'>
            <div class='col-12 col-md-8 text-center'>
              <h2>
                New Websites, Brand Consistency
              </h2>
              <p>
                PixelWatt developed a set of websites for both the <a href="http://vencomatic.ca/" target="_blank">Vencomatic North America</a> and <a href="http://davian.us/" target="_blank">Davian</a> brands based off a common visual language.
              </p>
            </div>
          </div>
        </div>
        <div class='owl-carousel owl-carousel-website'>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='705' src='<?php bloginfo('template_directory') ?>/images/vencomatic-web-01.png' width='1100'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='705' src='<?php bloginfo('template_directory') ?>/images/vencomatic-web-02.png' width='1100'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='705' src='<?php bloginfo('template_directory') ?>/images/vencomatic-web-03.png' width='1100'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/davian-web-01.png' width='1282'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-fluid' height='801' src='<?php bloginfo('template_directory') ?>/images/davian-web-02.png' width='1282'>
          </div>
        </div>
      </div>
      <hr>
      <!-- #vencomatic-brochures.casestudy-singlewide -->
      <!-- .container-fluid -->
      <!-- .row -->
      <!-- .col-12.text-center -->
      <!-- %h2 -->
      <!-- New Product Brochures -->
      <!-- %img.img-responsive{:src => 'images/vencomatic-brochures-product.png', :width => '1817', :height => '697'} -->
      <div class='casestudy-image parallax' id='vencomatic-type' data-jarallax data-speed="0.8" style="background-image: url(<?php bloginfo('template_directory') ?>/images/vencomatic-type.jpg);">
        <div class='casestudy-caption'>
          Brochure Typography
        </div>
      </div>
      <hr>
    </div>
    <div class='casestudy-contact'>
      <div class='container'>
        <div class='row'>
          <div class='col-12 col-sm-10 col-sm-offset-1 text-center'>
            <h2>
              PixelWatt helped Vencomatic rebrand itself.
              <strong>
                Let’s talk about what we could do for your business.
              </strong>
            </h2>
            <p>
              Fill out the form below to get the conversation started.
            </p>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
            	<?php the_content(); ?>
            
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>