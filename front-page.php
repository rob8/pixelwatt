<?php get_header(); ?>
	<div id="page-header">
		<?php echo pixelwatt_build_nav_container( 'default' ); ?>
	</div>

	<div id="page-footer" class="text-right">
		&copy; 2018 Rob Clark
	</div>

  	<div id="fullpage">

  		<?php
  			//$prefix = '_front_';
  			$intro_headline_l1 = get_post_meta( $id, '_front_intro_headline_l1', true );
			$intro_headline_l2 = get_post_meta( $id, '_front_intro_headline_l2', true );
			$intro_headline = '<h1>' . esc_html( $intro_headline_l1 ) . ' ' . ( ! empty( $intro_headline_l2 ) ? '<strong>' . esc_html( $intro_headline_l2 ) . '</strong>' : '' ) . '</h1>';

			$intro_copy = get_post_meta( $id, '_front_intro_copy', true );
			$intro_copy = pixelwatt_filter_content( $intro_copy );

			$intro_items = get_post_meta( $id, '_front_intro_items', true );
  		?>
	
		<div id="front-intro" class="section">
	  		<div class="container-fluid">
				<div class="row align-items-center">
		  			<div class="col-12 col-lg-7 section-content order-2 order-lg-1">
						<div class="section-content-inner">
			  				<?php echo $intro_headline . $intro_copy; ?>
			  				
							<a class='btn btn-primary btn-lg moveDown'>Learn More <i class="fas fa-chevron-right icon-right"></i></a>
						</div>
		  			</div>
		  			<div class="col-12 col-lg-5 section-image order-1 order-lg-2">
		  				<div class="section-image-outer">
						<div class="section-image-inner">
			  				<div class="screen-overlay" data-mh="screen-group">
			  					<?php
			  						if ( ! empty( $intro_items ) ) {
			  							if ( is_array( $intro_items ) ) {
			  								echo '<div class="owl-carousel owl-theme">';
			  								foreach ( $intro_items as $item ) {
			  									echo ( ! empty( $item['image'] ) ? '<div class="item screen" data-mh="screen-group" style="background-image: url(\'' . $item['image'] . '\');">' : '' );
			  									if ( isset( $item['page_id'] ) ) {
			  										if ( ! empty( $item['page_id'] ) ) {
			  											echo '
			  												<a class="align-items-center d-flex" href="' . get_the_permalink( $item['page_id'] ) . '"><span class="w-100 text-center"><i class="far fa-search"></i> View This Project</span></a>
			  											';
			  										}
			  									}
			  									echo ( ! empty( $item['image'] ) ? '</div>' : '' );
			  								}
			  								echo '</div>';
			  							}
			  						}
			  					?>
								
			  				</div>
			  				<img src="<?php bloginfo('template_directory') ?>/images/front-frame.png" class="img-fluid d-none d-lg-inline">
			  				<img src="<?php bloginfo('template_directory') ?>/images/front-frame-mobile.png" class="img-fluid d-inline d-lg-none">

						</div>
						</div>
		  			</div>
				</div>
	  		</div>
	  		<a class='moveDown footer-link'>About PixelWatt</a> 
		</div>

		<?php
			$services_intro = get_post_meta( $id, '_front_services_intro', true );
			$services_intro = ( ! empty( $services_intro ) ? '<h3>' . pixelwatt_format_tags( esc_html( $services_intro ) ) . '</h3>' : '' );

			$services_headline = get_post_meta( $id, '_front_services_headline', true );
			$services_headline = ( ! empty( $services_headline ) ? '<h2>' . pixelwatt_format_tags( esc_html( $services_headline ) ) . '</h2>' : '' );

			$services_items = get_post_meta( $id, '_front_services_items', true );
		?>

		<div id="front-services" class="section">
	  		<div class="container-fluid">
				<div class="row justify-content-center">
		  			<div class="col-12 col-sm-11 col-xl-10 section-content">
						<div class="section-content-inner">
			  				<?php 
			  					echo $services_intro . $services_headline; 

			  					if ( ! empty( $services_items ) ) {
			  						if ( is_array( $services_items ) ) {
			  							echo '<div class="row items">';
			  							$i = 1;
			  							foreach ( $services_items as $item ) {
			  								
			  								if ( ! empty($item) ) {
			  									$title = get_post_meta( $item, '_service_title', true );
			  									$title_modal = ( ! empty( $title ) ? '<h5 class="modal-title" id="serviceModal' . $i . 'Label">' . pixelwatt_format_tags( esc_html( $title ) ) . '</h5>' : '' );
			  									$title = ( ! empty( $title ) ? '<h4>' . pixelwatt_format_tags( esc_html( $title ) ) . '</h4>' : '' );
			  									

			  									$desc = get_post_meta( $item, '_service_short_desc', true );
			  									$desc = pixelwatt_filter_content( $desc );

			  									$full = get_post_meta( $item, '_service_full_desc', true );
			  									$full = pixelwatt_filter_content( $full );

			  									$icon = get_post_meta( $item, '_service_front_icon', true );
			  									$icon_w = get_post_meta( $item, '_service_front_icon_w', true );
			  									$icon_h = get_post_meta( $item, '_service_front_icon_h', true );

			  									if ( ( ! empty( $icon ) ) && ( ! empty( $icon_w ) ) && ( ! empty( $icon_h ) ) ) {
			  										echo '
			  											<style>
			  												span.si-' . $i . ' { background-image: url(' . $icon . '); background-size: ' . $icon_w . ' ' . $icon_h . '; min-height: ' . $icon_h . '; }
			  											</style>

			  											<div class="col-12 col-md-6 item">
										  					<div class="row">
																<div class="col-12">
											  						<span class="si-' . $i . ' si-item">
																		' . $title . $desc . '
																		' . ( ! empty( $full ) ? '<a class="learn-more" data-toggle="modal" data-target="#serviceModal' . $i . '">Learn More <i class="fas fa-chevron-right icon-right"></i></a>' : '' ) . '
											  						</span>
																</div>
										  					</div>
														</div> <!-- item -->
			  										';
			  									}

			  									if ( ! empty( $full ) ) {
			  										echo '
			  											<div class="modal serviceModal fade" id="serviceModal' . $i . '" tabindex="-1" role="dialog" aria-labelledby="serviceModal' . $i . 'Label" aria-hidden="true">
														  <div class="modal-dialog modal-lg" role="document">
														    <div class="modal-content">
														      <div class="modal-header">
														        ' . $title_modal . '
														        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
														          <span aria-hidden="true">&times;</span>
														        </button>
														      </div>
														      <div class="modal-body">
														        ' . $full . '
														      </div>
														    </div>
														  </div>
														</div>
			  										';
			  									}

			  									$i++;
			  								}
			  							}

			  							echo '</div>';
			  						}
			  					}
			  				?>

						</div>
		  			</div>
				</div>
	  		</div> 
	  		<a class='moveDown footer-link'>Recent Work</a> 
		</div>

		<?php
			$recent_items = get_post_meta( $post->ID, '_front_recent_items', true );

			if ( ! empty( $recent_items ) ) {
			  	if ( is_array( $recent_items ) ) {
			  		echo '<div class="section" id="front-recent">';

			  		foreach( $recent_items as $item ) {
			  			$headline = ( isset( $item['name'] ) ? ( ! empty( $item['name'] ) ? '<h2>' . $item['name'] . '</h2>' : '' ) : '' );
			  			$bg = ( isset( $item['image'] ) ? ( ! empty( $item['image'] ) ? ' style="background-image: url(\'' . $item['image'] . '\');"' : '' ) : '' );
			  			$btn = ( isset( $item['page_id'] ) ? ( ! empty( $item['page_id'] ) ? '<a class="btn btn-primary btn-lg" href="' . get_the_permalink( $item['page_id'] ) . '">Learn More <i class="fas fa-chevron-right icon-right"></i></a>' : '' ) : '' );
			  			$logo = ( isset( $item['logo'] ) ? ( ! empty( $item['logo'] ) ? '<img src="' . $item['logo'] . '" class="img-fluid d-inline d-md-none">' : '' ) : '' );

			  			echo '
			  				<div class="slide"' . $bg . '>
								<div class="container-fluid">
						  			<div class="row">
										<div class="col-10 offset-1 col-md-5 offset-md-1">
							  				' . $logo . $headline . $btn . '
										</div>
						  			</div>
								</div>
								<a class="moveDown footer-link">Say Hello!</a>
					  		</div>
			  			';
			  		}

			  		echo '</div>';
			  	}
			}
		?>

		<div id="front-contact" class="section bg-primary">
	  		<div class="section-inner">
				<div class="container-fluid section-container no-gutters">
		  			<div class="row">
						<div class="col-12 col-lg-6 section-col-6 section-map">
			  				<?php
			  				if ( function_exists('snapmap_build_single') ) {
				  				$marker = array( 'url' => esc_url( get_template_directory_uri() ) . '/images/map-pin.png', 'size' => '57,82', 'origin' => '0,0', 'anchor' => '29,80' );
				  				echo snapmap_build_single( $post->ID, '100%', '100vh', '15', false, $marker ); 
			  				}
			  				?>
						</div>
						<div class="col-12 col-lg-6 section-col-6 section-form align-items-center d-flex">
			  				<div class="section-form-inner w-100">
								<?php
									$ccon = get_post_meta( $post->ID, '_front_contact_content', true );
									echo pixelwatt_filter_content( $ccon );
								?>
			  				</div>
						</div>
		  			</div>
				</div>
	  		</div>
		</div>
  	
  	</div>

  <?php wp_footer(); ?>
  </body>
</html>
