<?php get_header(); ?>
    <header id='header-thoughts'>
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-xs-6'>
            <a class='logo' href='/' id='logo'>
              PixelWatt
            </a>
          </div>
          <div class='col-xs-6 text-right hamburger'>
            <span class='overlay-toggle' onclick='openNav()'>
              <span class='fa-stack fa-lg'>
                <i class='fa fa-circle fa-stack-2x'></i>
                <i class='fa fa-bars fa-stack-1x fa-inverse'></i>
              </span>
            </span>
          </div>
        </div>
      </div>
    </header>
    <div class="thoughts-content thoughts-listing">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                    
                        <div class="thought-item">
                            
                    
                    	    <?php the_post_thumbnail('blog', ['class' => 'img-responsive', 'title' => 'Feature image']); ?>
                    	    
                    	    <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                <h2 class="thought-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                                <span class="thought-date">Posted on <?php the_time('F j, Y'); ?></span>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink(); ?>" class="btn btn-lg btn-primary">Read More</a>
                            </div>
                    	    </div>
                        
                                
                        </div>
                    
                    <?php endwhile; else: ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                    <?php endif; ?>
                    
                </div>
            </div>
        </div>
    </div>
    
    <?php echo pixelwatt_build_blog_form(); ?>

<?php get_footer(); ?>
