<?php 
/* 
Template Name: Work
*/ 
?>

<?php get_header(); ?>
    <header class='header-work'>
      <?php echo pixelwatt_build_nav_container(); ?>
      <div class="container">
          <div class="row headline-row align-items-center">
              <div class="col-12 text-center">
                  <h1>Recent Work</h1>
              </div>
          </div>
      </div>
    </header>
    <div id="work-intro">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-md-11">
            <p class="lead"><strong>PixelWatt translates brands into bleeding-edge digital properties by building fully-custom solutions from the ground up.</strong></p>
            <p>You won't find any cookie-cutter templates or stock themes below. Instead, you'll find sites built specifically for each client and their specific goals.</p>
          </div>
        </div>
      </div>
    </div>

    <?php

      $items = get_post_meta( $post->ID, '_work_items', true );

      if ( ! empty( $items ) ) {
        if ( is_array( $items ) ) {
          echo '
            <div class="work-grid">
              <div class="container-fluid no-gutters">
                <div class="row">
          ';

          foreach ( $items as $item ) {
            echo '
              <a class="col-16 col-sm-6 col-lg-3 work-item" style="background-image: url(' . $item['image'] . ');" href="' . get_the_permalink( $item['page_id'] ) . '">>
                <div class="work-item-inner align-items-center d-flex" >
                  <div class="w-100 text-center">
                    <img src="' . $item['logo'] . '" class="img-fluid work-item-logo">
                  </div>
                </div>
                <div class="work-item-image">
                  <img src="' . esc_url( get_template_directory_uri() ) . '/images/spacer.png' . '" class="img-fluid" width="1000" height="1000">
                </div>
              </a>
            ';
          }

          echo '
                </div>
              </div>
            </div>
          ';
        }
      }

    ?>

    

    <?php echo pixelwatt_build_cs_form( $post->ID ); ?>
    
<?php get_footer(); ?>
