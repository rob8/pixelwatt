<?php
    
    //-----------------------------------------------------
	// Theme and Post Support
	//-----------------------------------------------------
	
	add_theme_support( 'title-tag' );			// Add theme support for the title tag
	add_theme_support( 'post-thumbnails' );		// Add theme support for post thumbnails
	
	
	//-----------------------------------------------------
	// Enable and Specify Menus
	//-----------------------------------------------------
	
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'primary' => 'Primary',
			)
		);
	}
	
	
	//-----------------------------------------------------
	// Load theme styles and scripts
	//-----------------------------------------------------
	
	function pixelwatt_scripts() {
        wp_enqueue_style( 'fontawesome', 'https://pro.fontawesome.com/releases/v5.1.0/css/all.css' );
		wp_enqueue_style( 'pixelwatt', get_stylesheet_uri(), '', '2.0.0' );
        wp_enqueue_script( 'scripts', get_template_directory_uri() . '/assets/js/scripts.min.js', array( 'jquery' ), '2.0.0', true );

        if (!is_admin()) {
            wp_deregister_script('jquery');
            wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js', true, '2.1.1');
            wp_enqueue_script('jquery');
        }
	}
	
	add_action( 'wp_enqueue_scripts', 'pixelwatt_scripts' );
	
	
	
    
    add_image_size( 'blog', 920, 530, true );
    
    
    
    //======================================================================
    // 2. THEME CUSTOMIZATION
    //======================================================================
    
    
        /**
        * Contains methods for customizing the theme customization screen.
        * 
        * @link http://codex.wordpress.org/Theme_Customization_API
        * @since MyTheme 1.0
        */
        
        class pixelwatt_Customize {
            /**
            * This hooks into 'customize_register' (available as of WP 3.4) and allows
            * you to add new sections and controls to the Theme Customize screen.
            * 
            * Note: To enable instant preview, we have to actually write a bit of custom
            * javascript. See live_preview() for more.
            *  
            * @see add_action('customize_register',$func)
            * @param \WP_Customize_Manager $wp_customize
            * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
            * @since MyTheme 1.0
            */
            
            public static function register ( $wp_customize ) {
                
                $wp_customize->add_section( 'social_options', 
                    array(
                        'title' => __( 'Social Media', 'pixelwatt' ), //Visible title of section
                        'priority' => 35, //Determines what order this appears in
                        'capability' => 'edit_theme_options', //Capability needed to tweak
                        'description' => __('Specify social media accounts to display on the site.', 'pixelwatt'), //Descriptive tooltip
                    ) 
                );
                
                $wp_customize->add_section( 'blog_options', 
                    array(
                        'title' => __( 'Blog Options', 'pixelwatt' ), //Visible title of section
                        'priority' => 35, //Determines what order this appears in
                        'capability' => 'edit_theme_options', //Capability needed to tweak
                    ) 
                );
                
                //2. Register new settings to the WP database...
            
                $wp_customize->add_setting( 'social_twitter', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_setting( 'social_facebook', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_setting( 'social_instagram', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_setting( 'social_dribbble', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_setting( 'social_github', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_setting( 'blog_form', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                    array(
                        'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                        'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                        'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                    ) 
                );
                
                $wp_customize->add_control('pixelwatt_social_twitter', array(
                    'label'      => __('Twitter', 'pixelwatt'),
                    'section'    => 'social_options',
                    'settings'   => 'social_twitter',
                ));
                
                $wp_customize->add_control('pixelwatt_social_facebook', array(
                    'label'      => __('Facebook', 'pixelwatt'),
                    'section'    => 'social_options',
                    'settings'   => 'social_facebook',
                ));
                
                $wp_customize->add_control('pixelwatt_social_instagram', array(
                    'label'      => __('Instagram', 'pixelwatt'),
                    'section'    => 'social_options',
                    'settings'   => 'social_instagram',
                ));
                
                $wp_customize->add_control('pixelwatt_social_dribbble', array(
                    'label'      => __('Dribbble', 'pixelwatt'),
                    'section'    => 'social_options',
                    'settings'   => 'social_dribbble',
                ));
                
                $wp_customize->add_control('pixelwatt_social_github', array(
                    'label'      => __('Github', 'pixelwatt'),
                    'section'    => 'social_options',
                    'settings'   => 'social_github',
                ));
                
                $wp_customize->add_control(
                    	'pixelwatt_blog_form', 
                    	array(
                    		'label'    => __( 'Blog Form', 'pixelwatt' ),
                    		'section'  => 'blog_options',
                    		'settings' => 'blog_form',
                    		'type'     => 'radio',
                    		'choices'  => pixelwatt_nf_array()
                    	)
                );
                
                
            }
    
        /**
        * This will output the custom WordPress settings to the live theme's WP head.
        * 
        * Used by hook: 'wp_head'
        * 
        * @see add_action('wp_head',$func)
        * @since MyTheme 1.0
        */
        
        public static function header_output() {
        ?>
            <!--Customizer CSS--> 
            <style type="text/css">
                
                
                
            </style> 
            <!--/Customizer CSS-->
        <?php
        }
       
        /**
        * This outputs the javascript needed to automate the live settings preview.
        * Also keep in mind that this function isn't necessary unless your settings 
        * are using 'transport'=>'postMessage' instead of the default 'transport'
        * => 'refresh'
        * 
        * Used by hook: 'customize_preview_init'
        * 
        * @see add_action('customize_preview_init',$func)
        * @since MyTheme 1.0
        */
        public static function live_preview() {
            wp_enqueue_script( 
                'h2l-themecustomizer', // Give the script a unique ID
                get_template_directory_uri() . '/js/theme-customizer.js', // Define the path to the JS file
                array(  'jquery', 'customize-preview' ), // Define dependencies
                '', // Define a version (optional) 
                true // Specify whether to put in footer (leave this true)
            );
        }
    
        /**
        * This will generate a line of CSS for use in header output. If the setting
        * ($mod_name) has no defined value, the CSS will not be output.
        * 
        * @uses get_theme_mod()
        * @param string $selector CSS selector
        * @param string $style The name of the CSS *property* to modify
        * @param string $mod_name The name of the 'theme_mod' option to fetch
        * @param string $prefix Optional. Anything that needs to be output before the CSS property
        * @param string $postfix Optional. Anything that needs to be output after the CSS property
        * @param bool $echo Optional. Whether to print directly to the page (default: true).
        * @return string Returns a single line of CSS with selectors and a property.
        * @since MyTheme 1.0
        */
        public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
            $return = '';
            $mod = get_theme_mod($mod_name);
            if ( ! empty( $mod ) ) {
                $return = sprintf('%s { %s:%s; }',
                $selector,
                $style,
                $prefix.$mod.$postfix
            );
                if ( $echo ) {
                    echo $return;
                }
            }
            return $return;
        }
        
        
    }
    
    // Setup the Theme Customizer settings and controls...
    add_action( 'customize_register' , array( 'pixelwatt_Customize' , 'register' ) );
    
    // Output custom CSS to live site
    add_action( 'wp_head' , array( 'pixelwatt_Customize' , 'header_output' ) );
    
    // Enqueue live preview javascript in Theme Customizer admin screen
    add_action( 'customize_preview_init' , array( 'pixelwatt_Customize' , 'live_preview' ) );
    
    
add_action( 'init', 'pixelwatt_service_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function pixelwatt_service_init() {
    $labels = array(
        'name'               => _x( 'Services', 'post type general name', 'your-plugin-textdomain' ),
        'singular_name'      => _x( 'Service', 'post type singular name', 'your-plugin-textdomain' ),
        'menu_name'          => _x( 'Services', 'admin menu', 'your-plugin-textdomain' ),
        'name_admin_bar'     => _x( 'Service', 'add new on admin bar', 'your-plugin-textdomain' ),
        'add_new'            => _x( 'Add New', 'service', 'your-plugin-textdomain' ),
        'add_new_item'       => __( 'Add New Service', 'your-plugin-textdomain' ),
        'new_item'           => __( 'New Service', 'your-plugin-textdomain' ),
        'edit_item'          => __( 'Edit Service', 'your-plugin-textdomain' ),
        'view_item'          => __( 'View Service', 'your-plugin-textdomain' ),
        'all_items'          => __( 'All Services', 'your-plugin-textdomain' ),
        'search_items'       => __( 'Search Services', 'your-plugin-textdomain' ),
        'parent_item_colon'  => __( 'Parent Services:', 'your-plugin-textdomain' ),
        'not_found'          => __( 'No services found.', 'your-plugin-textdomain' ),
        'not_found_in_trash' => __( 'No services found in Trash.', 'your-plugin-textdomain' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Services for use across the site.', 'your-plugin-textdomain' ),
        'public'             => false,
        'publicly_queryable' => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title' )
    );

    register_post_type( 'service', $args );
}


    
    function pixelwatt_social_links($accounts) {
    	    
    	    $output = '';
    	    
    	    foreach ($accounts as $account) {
        	    switch ($account) {
                case 'dribbble':
                    $service['url'] = get_theme_mod('social_dribbble');
                    $service['label'] = 'Dribbble';
                    $service['icon'] = 'fab fa-dribbble';
                    $service['class'] = 'dribbble';
                    break;
                case 'twitter':
                    $service['url'] = get_theme_mod('social_twitter');
                    $service['label'] = 'Twitter';
                    $service['icon'] = 'fab fa-twitter';
                    $service['class'] = 'twitter';
                    break;
                case 'facebook':
                    $service['url'] = get_theme_mod('social_facebook');
                    $service['label'] = 'Facebook';
                    $service['icon'] = 'fab fa-facebook';
                    $service['class'] = 'facebook';
                    break;
                case 'instagram':
                    $service['url'] = get_theme_mod('social_instagram');
                    $service['label'] = 'Instagram';
                    $service['icon'] = 'fab fa-instagram';
                    $service['class'] = 'instagram';
                    break;
                case 'github':
                    $service['url'] = get_theme_mod('social_github');
                    $service['label'] = 'Github';
                    $service['icon'] = 'fab fa-github-alt';
                    $service['class'] = 'github';
                    break;
                default:
                    break;
            }
                
            if (!empty($service['url'])) {  
                $output .= '
                    <li class="'.$service['class'].'">
                      <a href="'.$service['url'].'" target="_blank">
                        <i class="'.$service['icon'].'" aria-hidden="true"></i>
                        <span class="sr-only">
                          '.$service['label'].'
                        </span>
                      </a>
                    </li>
                ';
            } else {
                $output .= '
                    <li class="'.$service['class'].'" style="display: none; visibility: hidden;">
                        &nbsp;
                    </li>
                ';
            }
            
    	    }
    	    
    	    return $output;
	}

    function pixelwatt_build_nav_container( $style = 'inverted' ) {
        $output = '
            <div class="container-fluid nav-container nav-container-' . $style . '">
                <div class="row">
                    <div class="col-6">
                        <a class="logo-' . $style . '" href="' . site_url() . '" id="logo">
                            PixelWatt
                        </a>
                    </div>
                    <div class="col-6 text-right hamburger">
                        <span class="overlay-toggle" onclick="openNav()">
                            <span class="block-bars"><span class="line">&nbsp;</span><span class="line">&nbsp;</span><span class="line">&nbsp;</span></span> Menu
                        </span>
                    </div>
                </div>
            </div>
        ';

        return $output;
    }

    function pixelwatt_filter_content( $content ) {
        if ( ! empty( $content ) ) {
            $content = apply_filters( 'the_content', $content );
        }
        return $content;
    }
	
	
	function pixelwatt_nf_array() {
		
		$output;
		$output[] = "None";
		
		$forms = ninja_forms_get_all_forms();
		
		foreach ($forms as $form) {
			$output["{$form['id']}"] = $form['data']['title'];
		}
		
		return $output;
		
	}
	
	
	function pixelwatt_work_array() {
    	
    	$output;
		$output[] = "None";
		
		$args = array(
            'post_type'  => 'page',
            'meta_query' => array(
                array(
                    'key'   => 'is_work_item',
                    'value' => 'on',
                )
            )
        );
        $items = get_posts( $args );
        
        foreach ($items as $item) {
            $output["{$item->ID}"] = $item->post_title; 
        }
        
        return $output;
    	
	}

    function pixelwatt_format_tags($content) {
        $content = str_replace('[amp]', '<span class="amp">&amp;</span>', $content);
        $content = str_replace('[strong]', '<strong>', $content);
        $content = str_replace('[/strong]', '</strong>', $content);
        $content = str_replace('[br]', '<br>', $content);
        return $content;
    }
	
	function pixelwatt_build_blog_form() {
    	    
    	    $output = '';
    	    
    	    $form_id = get_theme_mod('blog_form');
    	    
    	    if (!empty($form_id)) {
        	    
        	    $output = '
        	    
        	        <div class="thoughts-contact">
                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
                        <h2>
                          Let\'s talk about how PixelWatt can help your business.
                        </h2>
                        <p>
                          Fill out the form below to get the conversation started.
                        </p>
                        '.do_shortcode( '[ninja_form id='.$form_id.']' ).'
                      </div>
                    </div>
                  </div>
                </div>
        	    
        	    ';
        	    
    	    }
    	    
    	    return $output;
    	    
	}
	
	
	function pixelwatt_build_front_header($id) {
    	
    	// Let's pull some meta values
    	$meta['headline'] = get_post_meta($id, 'front_header_headline', true);
    	$meta['btn_label'] = get_post_meta($id, 'front_header_btn_label', true);
    	$meta['btn_url'] = get_post_meta($id, 'front_header_btn_url', true);
    	$meta['image'] = get_post_meta($id, 'front_header_image_id', true);
    	$meta['background'] = get_post_meta($id, 'front_header_background', true);
    	
    	if (!empty($meta['headline'])) {
        	$meta['headline'] = apply_filters('the_content',$meta['headline']);
    	}
    	
    	$output = '
    	      <div class="container header-content text-center">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="row">
                      <div class="col-xs-12 col-md-8 col-md-offset-2">
                        '.( !empty($meta['headline']) ? $meta['headline'] : '').'
                        <a class="btn btn-primary btn-lg" href="'.( !empty($meta['btn_url']) ? $meta['btn_url'] : '#').'">
                          '.( !empty($meta['btn_label']) ? $meta['btn_label'] : '').'
                        </a>
                        <br><br>
                      </div>
                    </div>
                    <div class="header-image">
                      '.( !empty($meta['image']) ? wp_get_attachment_image( $meta['image'], 'full', "", array( "class" => "img-responsive" ) ) : '').'
                    </div>
                  </div>
                </div>
              </div>
    	';
    	
    	return $output;
    	
	}
	
	function pixelwatt_build_front_highlighted($id) {
    	
    	$output;
    	
    	$i = 1;
    	
    	while ($i <= 2) {
        	
        	$item_id = get_post_meta($id, 'front_work_item'.$i, 'true');
        	
        	$item['headline'] = get_post_meta($item_id, 'page_work_headline', 'true');
        	$item['subtitle'] = get_post_meta($item_id, 'page_work_subtitle', 'true');
        	$item['background'] = get_post_meta($item_id, 'page_work_background_sm', 'true');
        	$item['url'] = get_the_permalink($item_id);
        	
        	$output .= '
        	
        	    <div class="col-xs-12 col-md-6 recent-item"'.( !empty($item['background']) ? ' style="background-image: url('.$item['background'].');"' : '' ).'>
                    <div class="recent-inner">
                        '.( !empty($item['headline']) ? '<h3>'.$item['headline'].'</h3>' : '').'
                        '.( !empty($item['subtitle']) ? '<p class="subtitle">'.$item['subtitle'].'</p>' : '').'
                        <a class="btn btn-primary btn-lg" href="'.$item['url'].'">Read More</a>
                    </div>
                </div>
        	
        	';
        	
        	$i++;
    	}
    	
    	return $output;
    	
	}
	
	function pixelwatt_build_front_services($id) {
    	
    	$output;
    	
    	$services = get_post_meta($id, 'front_services_items', true);
    	
    	foreach ($services as $service) {
        	$output .= '
        	    <div class="col-xs-12 col-sm-6 col-md-4 service">
        	      <div class="image-container" data-mh="service-group">
                    <div class="inner">
                      '.( !empty($service['icon']) ? '<img src="'.$service['icon'].'">' : '').'
                    </div>
                  </div>
                  '.( !empty($service['title']) ? '<h4>'.$service['title'].'</h4>' : '').'
                </div>
        	';
    	}
    	
    	return $output;
    	
	}
	
	function pixelwatt_build_contact_map_info($id) {
    	
    	$output = '';
    	
    	$content = get_post_meta($id, 'page_contact_info', true);
    	
    	if (!empty($content)) {
    	    
    	    $content = apply_filters('the_content',$content);
            $output = $content;
    	
    	}
    	
    	return $content;
    	
	}

    function pixelwatt_build_post_array( $type ) {
            
        $args = array(
            'post_type' => $type,
            'posts_per_page' => -1
        );
        
        
        $options = get_posts($args);
        $output = '';
        
        foreach ($options as $option) {
            $output["{$option->ID}"] = $option->post_title;
        }
        
        return $output;
        
    }

function pixelwatt_build_cs( $id, $key ) {
    $output = '';

    switch ($key) {
        case 'digium':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array(  'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array(  'title' => 'Wordpress &amp; Drupal Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array(  'title' => 'Security &amp; Backups ' ,'icon' => 'images/casestudy-service-security.png' );
            $output .= pixelwatt_build_cs_services( $services );

            // Build slider
            $s_head = '
                <h2>Poundkey: A Universal Framework</h2>

                <p>Ultimately, I created Poundkey, a framework built to fit any site while still providing enough flexibility to work for both Digium and Asterisk brands. Below are excerpts from the <a href="http://www.asterisk.org/">Asterisk</a>, <a href="http://www.asterisk.org/community/astricon-user-conference">AstriCon</a>, and <a href="https://digium.com">Digium</a> websites that utilize the framework.</p>
            ';
            
            $s_items[] = array( 'images/digium-web-01.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-02.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-03.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-04.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-05.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-06.jpg', '1278', '740' );
            $s_items[] = array( 'images/digium-web-07.jpg', '1278', '740' );
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            // Build Form
            $output .= pixelwatt_build_cs_form( $id );

            break;
        case 'cityofhsv':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );


            // Build services
            $services[] = array(  'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array(  'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array(  'title' => 'Security &amp; Maintenance ' ,'icon' => 'images/casestudy-service-security.png' );
            $output .= pixelwatt_build_cs_services( $services );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/cityofhsv-quote.jpg', 'City Blog Concept Artwork', '0.9', $key . '-quote'  );

            // Build slider
            $s_head = '
                <h2>Ongoing Expansion</h2>
                <p>PixelWatt continues to evolve the City of Huntsville\'s digital presence, which has grown to include multiple Wordpress sites over the last few years.</p>
            ';
            $s_items[] = array( 'images/huntsville-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-02a.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-04.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-05.jpg', '1400', '900' );
            $s_items[] = array( 'images/huntsville-web-06.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-03.jpg', '1400', '900' );
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/cityofhsv-modal.jpg', 'City Blog Author Modal', '0.9', $key . '-modal'  );

            // Build Form
            $output .= pixelwatt_build_cs_form( $id );
            break;
        case 'vencomatic':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array( 'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array( 'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array( 'title' => 'Responsive Email', 'icon' => 'images/casestudy-service-email.png' );
            $services[] = array( 'title' => 'Promotional Materials', 'icon' => 'images/casestudy-service-promo-materials.png' );
            $output .= pixelwatt_build_cs_services( $services );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/vencomatic-tradeshow.jpg', 'Showroom Product Posters', '0.8', $key . '-tradeshow'  );

            // Build slider
            $s_head = '
                <h2>New Websites, Brand Consistency</h2>

                <p>PixelWatt developed a set of websites for both the <a href="http://vencomatic.ca/" target="_blank">Vencomatic North America</a> and <a href="http://davian.us/" target="_blank">Davian</a> brands based off a common visual language.</p>
            ';
            $s_items[] = array( 'images/vencomatic-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/vencomatic-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/vencomatic-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/vencomatic-web-04.jpg', '1400', '900' );
            $s_items[] = array( 'images/davian-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/davian-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/davian-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/davian-web-04.jpg', '1400', '900' );
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '_website' );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/vencomatic-type.jpg', 'Brochure Typography', '0.8', $key . '-type'  );

            $output .= pixelwatt_build_cs_form( $id );
            break;
        case 'sexton':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array( 'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array( 'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array( 'title' => 'Brand Design', 'icon' => 'images/casestudy-service-promo-materials.png' );
            $output .= pixelwatt_build_cs_services( $services );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/sexton-logo.jpg', 'New Logo Design', '0.8', $key . '-logo'  );

            // Build slider
            $s_head = '
                <h2>Two Sites Under One Roof</h2>

                <p>PixelWatt created a multisite solution to provide Sexton with the ability to administrate both sites through a unified Wordpress dashboard. A shared custom theme powers both sites and allows admins to easily control each site\'s branding and overall color scheme. While the red scheme was ultimately used for both, two more options are available to instantly use via a custom themeing system (color variations detailed at the bottom of this page).</p>
            ';
            $s_items[] = array( 'images/sexton-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/sexton-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/sexton-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/sexton-web-04.jpg', '1400', '900' );
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            // Build full-width image
            $output .= pixelwatt_build_cs_image( 'images/sexton-colors.jpg', 'Selectable Color Variations', '0.8', $key . '-colors'  );

            $output .= pixelwatt_build_cs_form( $id );
            break;
        case 'maryna':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array( 'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array( 'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array( 'title' => 'Brand Design', 'icon' => 'images/casestudy-service-promo-materials.png' );
            $output .= pixelwatt_build_cs_services( $services );

            // Build slider
            $s_head = '
                <h2>Showcasing Great Hair</h2>

                <p>PixelWatt designed and developed a website for Maryna that put examples of her work at center stage, whether showcasing it through large header images or inside blog posts detailing styles created for past events.</p>
            ';
            $s_items[] = array( 'images/maryna-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/maryna-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/maryna-web-05.jpg', '1400', '900' );
            $s_items[] = array( 'images/maryna-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/maryna-web-04.jpg', '1400', '900' );
            
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            $output .= pixelwatt_build_cs_form( $id );
            break;
        case 'schrimsher':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array(  'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array(  'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array(  'title' => 'Security &amp; Maintenance ' ,'icon' => 'images/casestudy-service-security.png' );
            $output .= pixelwatt_build_cs_services( $services );

            $s_head = '
                <h2>A Multisite Solution</h2>

                <p>PixelWatt created a multisite solution that allows Schrimsher Company to administrate both websites through a unified control panel. A shared custom theme powers both sites, with site-specific options available to admins, based on which site is being edited.</p>
            ';
            $s_items[] = array( 'images/schrimsher-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/schrimsher-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/schrimsher-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/schrimsher-web-04.jpg', '1400', '900' );
            $s_items[] = array( 'images/schrimsher-web-06.jpg', '1400', '900' );
            
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            $output .= pixelwatt_build_cs_form( $id );

            break;
        case 'homegrown':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array(  'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array(  'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array(  'title' => 'Security &amp; Maintenance ' ,'icon' => 'images/casestudy-service-security.png' );
            $output .= pixelwatt_build_cs_services( $services );

            $s_head = '
                <h2>An Integrated Solution</h2>

                <p>PixelWatt created a custom theme for Homegrown that empowers admins to easily maintain a calendar of upcoming events and associated landing pages. Through a custom integration, admins can also enable ticket sales for each event using the SquadUp ticketing platform.</p>
            ';
            $s_items[] = array( 'images/homegrown-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/homegrown-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/homegrown-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/homegrown-web-04.jpg', '1400', '900' );
            $s_items[] = array( 'images/homegrown-web-05.jpg', '1400', '900' );
            
            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            $output .= pixelwatt_build_cs_form( $id );

            break;
        case 'hpd':
            // Build header
            $output .= pixelwatt_build_cs_header( $id );

            // Build services
            $services[] = array(  'title' => 'Website Design' ,'icon' => 'images/casestudy-service-webdesign.png' );
            $services[] = array(  'title' => 'Wordpress Development' ,'icon' => 'images/casestudy-service-webdev.png' );
            $services[] = array(  'title' => 'Security &amp; Maintenance ' ,'icon' => 'images/casestudy-service-security.png' );
            $output .= pixelwatt_build_cs_services( $services );

            $s_head = '
                <h2>A Call To Be The Best</h2>

                <p>With videos produced by Red Brick Strategies and a new centralized UI kit built by PixelWatt for the City of Huntsville’s multiple WordPress properties, PixelWatt designed and developed a website optimized for reaching candidates through traditional search and social media promotion.</p>
            ';
            $s_items[] = array( 'images/hpd-web-01.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-02.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-01a.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-03.jpg', '1400', '900' );
            $s_items[] = array( 'images/hpd-web-04.jpg', '1400', '900' );

            $output .= pixelwatt_build_cs_slider( $s_head, $s_items, $key . '-website' );

            $output .= pixelwatt_build_cs_form( $id );

            break;
        default:
            # code...
            break;
    }

    return $output;
}

function pixelwatt_build_cs_image( $image, $caption, $speed = false, $id ) {
    $output = '
      <div class="casestudy-image' . ( $speed ? ' jarallax' : '' ) . '" id="' . $id . '" ' . ( $speed ? ' data-jarallax data-speed="' . $speed . '" ' : '' ) . 'style="background-image: url(' . get_bloginfo("template_directory") . '/' . $image . ');">
        <div class="casestudy-caption">
          ' . $caption . '
        </div>
      </div>
      <hr>  
    ';

    return $output;
}

function pixelwatt_build_cs_form( $id ) {
    $form_id = get_post_meta( $id, '_casestudy_form_id', true );
    $form_l1 = get_post_meta( $id, '_casestudy_form_l1', true );
    $form_l2 = get_post_meta( $id, '_casestudy_form_l2', true );

    return '
        <div class="casestudy-contact">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-12 col-sm-11 col-md-10 text-center">
                <h2>
                  ' . esc_html( $form_l1 ) . '
                  <strong>
                    ' . esc_html( $form_l2 ) . '
                  </strong>
                </h2>
                ' . do_shortcode( '[ninja_form id=' . $form_id . ']' ) . '
              </div>
            </div>
          </div>
          <div class="casestudy-footer">
            <div class="container-fluid">
              <div class="row">

                <div class="col-12 col-md-6 copyright">
                  &copy; 2018 Rob Clark
                </div>

                <div class="col-12 col-md-6 text-right">
                  <ul class="social-icons">
                  ' . pixelwatt_social_links( array('twitter','facebook','instagram','dribbble','github') ) . '
                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>
    ';
}

function pixelwatt_build_cs_slider( $head, $items, $id ) {
    $output = '
        <div class="casestudy-website" id="' . $id . '">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-12 col-md-10 text-center">
                ' . $head . '
              </div>
            </div>
          </div>
          <div class="owl-carousel owl-carousel-website">
    ';

    foreach ( $items as $item ) {
        $output .= '
            <div class="item">
              
              <span class="item-image d-inline-block"><img class="img-fluid" src="' . get_bloginfo("template_directory") . '/' . $item[0] . '" width="' . $item[1] . '" height="' . $item[2] . '" ></span>

              <a class="next-website d-block">
                &nbsp;
              </a>
              <a class="prev-website d-block">
                &nbsp;
              </a>
            </div>
        ';
    }

    $output .= '
          </div>
        </div>
    ';

    return $output;
}

function pixelwatt_build_cs_services( $services ) {
    $output = '
        <div class="casestudy-content">
          <div class="casestudy-services">
            <div class="container-fluid">
              <div class="row justify-content-center">
                <div class="col-12 col-md-11">
                    <div class="row justify-content-between">
                        <div class="col-12 col-md-7">
                            <h2>About This Project</h3>
                            ' . pixelwatt_filter_content( get_the_content( $id ) ) . '
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="row">
                                <div class="col-12">
                                    <h2>Services</h2>
                                </div>';

    $i = 1;
    foreach ($services as $service) {
        $output .= '
            <style>
                #cs-service-' . $i . ' h4:before { background-image: url(' . get_bloginfo("template_directory") . '/' . $service['icon'] . '); }
            </style>
            <div class="col-12 service" id="cs-service-' . $i . '">
              
              <h4>
                ' . $service['title'] . '
              </h4>
            </div>
        ';
        $i++;
    }

    $output .= '
                
                </div></div></div></div>
              </div>
            </div>
          </div>
          <hr>
    ';

    return $output;
}

function pixelwatt_build_cs_header( $id ) {
    $headline = get_post_meta( $id, '_casestudy_headline', true );
    $client = get_post_meta( $id, '_casestudy_client', true );
    $introduction = get_post_meta( $id, '_casestudy_introduction', true );
    $bg = get_post_meta( $id, '_casestudy_bg_id', true );
    $key = get_post_meta( $id, '_casestudy_key', true );

    // Get video URL and set to default if no URL is provided
    $video_url = get_post_meta( $id, '_casestudy_header_video_url', true );

    $video_startat = get_post_meta( $id, '_casestudy_header_video_url_startat', true );
    $video_endat = get_post_meta( $id, '_casestudy_header_video_url_endat', true );

    $video_bg = ( ! empty( $bg ) ? wp_get_attachment_image_url( $bg, 'full' ) : '' );
            

    $output = '
    <script>
        jQuery(function(){
      jQuery("#P1").vimeo_player();
    });
    </script>
    <header class="header-casestudy jarallax" id="header-' . $key . '"' . ( empty( $video_url ) ? ( ! empty( $video_bg ) ? ' data-jarallax="" data-speed="0.4" style="background-image: url(' . $video_bg . ');"' : '' ) : '' ) . '>
    ';

    if ( ! empty($video_url) ) {
        $output .= '
            <div id="P1" class="player" data-property="{videoURL:\'' . $video_url . '\',containment:\'.header-casestudy\',showControls:false,showYTLogo:false,' . ( ! empty( $video_startat ) ? 'startAt:' . $video_startat . ',' : '' ) . ( ! empty( $video_endat ) ? 'stopAt:' . $video_endat . ',' : '' ) . 'mute:true,autoPlay:true,loop:true,opacity:0.4,align:\'center,center\',stopMovieOnBlur:false'. ( ! empty( $video_bg ) ? ',mobileFallbackImage:\'' . $video_bg . '\'' : '' ) .'}"></div>
        ';
    }

    $output .= '
      ' . pixelwatt_build_nav_container() . '
      <div class="header-content-container">
        <div class="container header-content">
          <div class="row">
            <div class="col-12">
              <div class="row justify-content-center">
                <div class="col-12 col-md-10 text-center">
                  <h1>' . esc_html( $headline ) . '</h1>
                  <p class="subtitle">Client: ' . esc_html( $client ) . '</p>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <hr>
    ';

    return $output;
}
	
	/**
     * Include metabox on front page
     * @author Ed Townend
     * @link https://github.com/WebDevStudios/CMB2/wiki/Adding-your-own-show_on-filters
     *
     * @param bool $display
     * @param array $meta_box
     * @return bool display metabox
     */
    function ed_metabox_include_front_page( $display, $meta_box ) {
    	if ( ! isset( $meta_box['show_on']['key'] ) ) {
    		return $display;
    	}
    
    	if ( 'front-page' !== $meta_box['show_on']['key'] ) {
    		return $display;
    	}
    
    	$post_id = 0;
    
    	// If we're showing it based on ID, get the current ID
    	if ( isset( $_GET['post'] ) ) {
    		$post_id = $_GET['post'];
    	} elseif ( isset( $_POST['post_ID'] ) ) {
    		$post_id = $_POST['post_ID'];
    	}
    
    	if ( ! $post_id ) {
    		return false;
    	}
    
    	// Get ID of page set as front page, 0 if there isn't one
    	$front_page = get_option( 'page_on_front' );
    
    	// there is a front page set and we're on it!
    	return $post_id == $front_page;
    }
    add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );
	
	
    


    add_action( 'cmb2_admin_init', 'pixelwatt_register_front_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function pixelwatt_register_front_metabox() {
        $prefix = '_front_';
        /**
         * Sample metabox to demonstrate each field type included
         */
        $cmb_front = new_cmb2_box( array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Front Page Metabox', 'pixelwatt' ),
            'object_types'  => array( 'page', ), 
            'show_on' => array( 'key' => 'front-page', 'value' => '' ),
        ) );

        $cmb_front->add_field( array(
            'name'     => esc_html__( 'Intro Section', 'cmb2' ),
            'desc'     => esc_html__( 'Configure the intro section.', 'cmb2' ),
            'id'       => $prefix . 'intro_info',
            'type'     => 'title',
        ) );

        $cmb_front->add_field( array(
            'name'    => __( 'Headline (Line 1)', 'pixelwatt' ),
            'id'      => $prefix . 'intro_headline_l1',
            'type'    => 'text',
        ) );

        $cmb_front->add_field( array(
            'name'    => __( 'Headline (Line 2)', 'pixelwatt' ),
            'id'      => $prefix . 'intro_headline_l2',
            'type'    => 'text',
        ) );

        $cmb_front->add_field( array(
            'name'    => __( 'Copy', 'pixelwatt' ),
            'id'      => $prefix . 'intro_copy',
            'type'    => 'text',
        ) );

        $group_field_id = $cmb_front->add_field( array(
            'id'          => $prefix . 'intro_items',
            'type'        => 'group',
            'description' => esc_html__( 'Add website images for the screen below.', 'cmb2' ),
            'options'     => array(
                'group_title'   => esc_html__( 'Screen {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Screen', 'cmb2' ),
                'remove_button' => esc_html__( 'Remove Screen', 'cmb2' ),
                'sortable'      => true,
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );

        $cmb_front->add_group_field( $group_field_id, array(
            'name'       => esc_html__( 'Client Name', 'cmb2' ),
            'id'         => 'name',
            'type'       => 'text',
        ) );

        $cmb_front->add_group_field( $group_field_id, array(
            'name' => esc_html__( 'Image', 'cmb2' ),
            'id'   => 'image',
            'type' => 'file',
        ) );

        $cmb_front->add_group_field( $group_field_id, array(
            'name'             => esc_html__( 'Destination Page', 'cmb2' ),
            'id'               => 'page_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => pixelwatt_build_post_array( 'page' ),
        ) );

        $cmb_front->add_field( array(
            'name'     => esc_html__( 'Services Section', 'cmb2' ),
            'desc'     => esc_html__( 'Configure the services section.', 'cmb2' ),
            'id'       => $prefix . 'services_info',
            'type'     => 'title',
        ) );

        $cmb_front->add_field( array(
            'name'    => __( 'Intro', 'pixelwatt' ),
            'id'      => $prefix . 'services_intro',
            'type'    => 'textarea',
        ) );

        $cmb_front->add_field( array(
            'name'    => __( 'Headline', 'pixelwatt' ),
            'id'      => $prefix . 'services_headline',
            'type'    => 'text',
        ) );

        $cmb_front->add_field( array(
            'name'             => esc_html__( 'Services to Display', 'cmb2' ),
            'id'               => $prefix . 'services_items',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => pixelwatt_build_post_array( 'service' ),
            'repeatable'       => true
        ) );

        $cmb_front->add_field( array(
            'name'     => esc_html__( 'Recent Work Section', 'cmb2' ),
            'desc'     => esc_html__( 'Configure the Recent Work section.', 'cmb2' ),
            'id'       => $prefix . 'recent_info',
            'type'     => 'title',
        ) );

        $group_field_recent = $cmb_front->add_field( array(
            'id'          => $prefix . 'recent_items',
            'type'        => 'group',
            'description' => esc_html__( 'Add slides to the Recent Work slider below.', 'cmb2' ),
            'options'     => array(
                'group_title'   => esc_html__( 'Slide {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Slide', 'cmb2' ),
                'remove_button' => esc_html__( 'Remove Slide', 'cmb2' ),
                'sortable'      => true,
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );

        $cmb_front->add_group_field( $group_field_recent, array(
            'name'       => esc_html__( 'Headline', 'cmb2' ),
            'id'         => 'name',
            'type'       => 'text',
        ) );

        $cmb_front->add_group_field( $group_field_recent, array(
            'name' => esc_html__( 'Background Image', 'cmb2' ),
            'id'   => 'image',
            'type' => 'file',
        ) );

        $cmb_front->add_group_field( $group_field_recent, array(
            'name' => esc_html__( 'Logo (for mobile)', 'cmb2' ),
            'id'   => 'logo',
            'type' => 'file',
        ) );

        $cmb_front->add_group_field( $group_field_recent, array(
            'name'             => esc_html__( 'Destination Page', 'cmb2' ),
            'id'               => 'page_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => pixelwatt_build_post_array( 'page' ),
        ) );

        $cmb_front->add_field( array(
            'name'     => esc_html__( 'Contact Section', 'cmb2' ),
            'desc'     => esc_html__( 'Configure the Contact section.', 'cmb2' ),
            'id'       => $prefix . 'contact_info',
            'type'     => 'title',
        ) );

        $cmb_front->add_field( array(    
            'name' => 'Map Location',  
            'desc' => '',   
            'id' => 'front_map_location',   
            'limit_drawing' => true, 
            'type' => 'snapmap',   
        ) ); 

        $cmb_front->add_field( array(
            'name'    => __( 'Contact Content', 'pixelwatt' ),
            'id'      => $prefix . 'contact_content',
            'type'    => 'wysiwyg',
        ) );

    }

    
    add_action( 'cmb2_admin_init', 'pixelwatt_register_page_contact_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function pixelwatt_register_page_contact_metabox() {
    	$prefix = 'page_contact_';
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_work = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => __( 'Contact Page Options', 'pixelwatt' ),
    		'object_types'  => array( 'page', ),
    		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-contact.php' ),
    	) );
    	
    	$cmb_page_work->add_field( array(
    		'name'       => esc_html__( 'Location', 'pixelwatt' ),
    		'desc'       => esc_html__( 'Select the location of this project\'s trailhead on the map.', 'pixelwatt' ),
    		'id'         => $prefix . 'location',
    		'limit_drawing' => true,
    		'type'       => 'snapmap',
    	) );
    	
    	$cmb_page_work->add_field( array(
    		'name'    => 'Contact Info',
        	'id'      => $prefix . 'info',
        	'type'    => 'wysiwyg',
    		'options' => array(
        		'media_buttons' => true, // show insert/upload button(s)
        	    'textarea_rows' => 8, // rows="..."
        	    'teeny' => false, // output the minimal editor config used in Press This
        	),
    	) );
    	
    }

    add_action( 'cmb2_admin_init', 'pixelwatt_register_service_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function pixelwatt_register_service_metabox() {
        $prefix = '_service_';
        /**
         * Sample metabox to demonstrate each field type included
         */
        $cmb_service = new_cmb2_box( array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Service Options', 'pixelwatt' ),
            'object_types'  => array( 'service', ),
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Title', 'pixelwatt' ),
            'id'      => $prefix . 'title',
            'type'    => 'text',
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Short Description', 'pixelwatt' ),
            'id'      => $prefix . 'short_desc',
            'type'    => 'textarea',
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Full Description', 'pixelwatt' ),
            'id'      => $prefix . 'full_desc',
            'type'    => 'wysiwyg',
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Front Icon', 'pixelwatt' ),
            'id'      => $prefix . 'front_icon',
            'type'    => 'file',
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Front Icon Width', 'pixelwatt' ),
            'id'      => $prefix . 'front_icon_w',
            'type'    => 'text',
        ) );

        $cmb_service->add_field( array(
            'name'    => __( 'Front Icon Height', 'pixelwatt' ),
            'id'      => $prefix . 'front_icon_h',
            'type'    => 'text',
        ) );

    }
    
    
    
    add_action( 'cmb2_admin_init', 'pixelwatt_register_page_casestudy_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function pixelwatt_register_page_casestudy_metabox() {
    	$prefix = '_casestudy_';
    	/**
    	 * Sample metabox to demonstrate each field type included
    	 */
    	$cmb_page_work = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => __( 'Case Study Options', 'pixelwatt' ),
    		'object_types'  => array( 'page', ),
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-casestudy.php' ),
    	) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Key', 'pixelwatt' ),
            'id'      => $prefix . 'key',
            'type'    => 'text',
        ) );
    	
    	$cmb_page_work->add_field( array(
    		'name'    => __( 'Headline', 'pixelwatt' ),
    		'id'      => $prefix . 'headline',
    		'type'    => 'text',
    	) );
    	
    	$cmb_page_work->add_field( array(
    		'name'    => __( 'Client', 'pixelwatt' ),
    		'id'      => $prefix . 'client',
    		'type'    => 'text',
    	) );

        $cmb_page_work->add_field( array(
            'name' => esc_html__( 'Background Video URL', 'cmb2' ),
            'desc' => __( 'Specify the URL of the hosted video. <strong>This video must be hosted on Vimeo.</strong>', 'cmb2' ),
            'id'   => $prefix . 'header_video_url',
            'type' => 'text_url',
            'attributes'  => array(
                'placeholder' => 'https://vimeo.com/248033449',
            ),
        ) );

        $cmb_page_work->add_field( array(
            'name' => __( 'Background Video Start', 'theme-domain' ),
            'desc' => __( 'At what time should the video being playing from (enter 0 to play from the beginning)?', 'msft-newscenter' ),
            'id'   => $prefix . 'header_video_url_startat',
            'type' => 'text',
            'attributes' => array(
                'type' => 'number',
                'pattern' => '\d*',
            ),
            'sanitization_cb' => 'absint',
                'escape_cb'       => 'absint',
        ) );

        $cmb_page_work->add_field( array(
            'name' => __( 'Background Video End', 'theme-domain' ),
            'desc' => __( 'At what time should the video loop back to the beginning (enter 0 to play until the end before looping)?', 'msft-newscenter' ),
            'id'   => $prefix . 'header_video_url_endat',
            'type' => 'text',
            'attributes' => array(
                'type' => 'number',
                'pattern' => '\d*',
            ),
            'sanitization_cb' => 'absint',
                'escape_cb'       => 'absint',
        ) );
    	
    	$cmb_page_work->add_field( array(
    		'name'    => __( 'Background Image', 'pixelwatt' ),
    		'id'      => $prefix . 'bg',
    		'type'    => 'file',
    	) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form Headline (Line 1)', 'pixelwatt' ),
            'id'      => $prefix . 'form_l1',
            'type'    => 'text',
        ) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form Headline (Line 2)', 'pixelwatt' ),
            'id'      => $prefix . 'form_l2',
            'type'    => 'text',
        ) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form ID', 'pixelwatt' ),
            'id'      => $prefix . 'form_id',
            'type'    => 'text',
        ) );
    	
    }

    add_action( 'cmb2_admin_init', 'pixelwatt_register_page_rwork_metabox' );
    /**
     * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
     */
    function pixelwatt_register_page_rwork_metabox() {
        $prefix = '_work_';
        /**
         * Sample metabox to demonstrate each field type included
         */
        $cmb_page_work = new_cmb2_box( array(
            'id'            => $prefix . 'metabox',
            'title'         => __( 'Recent Work Options', 'pixelwatt' ),
            'object_types'  => array( 'page', ),
            'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-work.php' ),
        ) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form Headline (Line 1)', 'pixelwatt' ),
            'id'      => '_casestudy_form_l1',
            'type'    => 'text',
        ) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form Headline (Line 2)', 'pixelwatt' ),
            'id'      => '_casestudy_form_l2',
            'type'    => 'text',
        ) );

        $cmb_page_work->add_field( array(
            'name'    => __( 'Form ID', 'pixelwatt' ),
            'id'      => '_casestudy_form_id',
            'type'    => 'text',
        ) );

        $group_field_recent = $cmb_page_work->add_field( array(
            'id'          => $prefix . 'items',
            'type'        => 'group',
            'description' => esc_html__( 'Add slides to the Recent Work slider below.', 'cmb2' ),
            'options'     => array(
                'group_title'   => esc_html__( 'Item {#}', 'cmb2' ), // {#} gets replaced by row number
                'add_button'    => esc_html__( 'Add Another Item', 'cmb2' ),
                'remove_button' => esc_html__( 'Remove Item', 'cmb2' ),
                'sortable'      => true,
                'closed'     => true, // true to have the groups closed by default
            ),
        ) );

        $cmb_page_work->add_group_field( $group_field_recent, array(
            'name' => esc_html__( 'Background Image', 'cmb2' ),
            'id'   => 'image',
            'type' => 'file',
        ) );

        $cmb_page_work->add_group_field( $group_field_recent, array(
            'name' => esc_html__( 'Logo (for mobile)', 'cmb2' ),
            'id'   => 'logo',
            'type' => 'file',
        ) );

        $cmb_page_work->add_group_field( $group_field_recent, array(
            'name'             => esc_html__( 'Destination Page', 'cmb2' ),
            'id'               => 'page_id',
            'type'             => 'select',
            'show_option_none' => true,
            'options'          => pixelwatt_build_post_array( 'page' ),
        ) );

    }