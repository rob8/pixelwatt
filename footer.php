    <?php if ( ! is_page_template( 'page-template-casestudy.php' ) ) { ?>
    <!-- <footer>
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-12 col-sm-4'>
            <div class='inner' data-mh='footer-group'>
              <a class='logo-inverted-small' href='<?php echo site_url(); ?>' id='logo'>
                PixelWatt
              </a>
            </div>
          </div>
          <div class='col-12 col-sm-4 text-center'>
            <div class='inner' data-mh='footer-group'>
              <div class='inner-content'>
                &copy; Copyright 2017 Rob Clark
              </div>
            </div>
          </div>
          <div class='col-12 col-sm-4 text-right'>
            <div class='inner' data-mh='footer-group'>
              <div class='inner-content'>
                <div class='social-accounts'>
                  <ul>
                    <?php echo pixelwatt_social_links( array('twitter','facebook','dribbble','github') ); ?>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer> -->
    <?php } ?>
    <?php wp_footer(); ?>
    
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
      ga('create', 'UA-98808839-1', 'auto');
      ga('send', 'pageview');
    
    </script>
  </body>
</html>
