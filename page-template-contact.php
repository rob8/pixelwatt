<?php 
/* 
Template Name: Contact
*/ 
?>

<?php get_header(); ?>
    <div id="front-contact" class="bg-primary front-contact-standalone">
        <div class="nav-container-outer">
        <?php echo pixelwatt_build_nav_container( 'default' ); ?>
        </div>
        <div class="section-inner">
        <div class="container-fluid section-container no-gutters">
            <div class="row">
            <div class="col-12 col-lg-5 section-col-6 section-map">
                <?php
                if ( function_exists('snapmap_build_single') ) {
                  $frontpage_id = get_option( 'page_on_front' );
                  $marker = array( 'url' => esc_url( get_template_directory_uri() ) . '/images/map-pin.png', 'size' => '57,82', 'origin' => '0,0', 'anchor' => '29,80' );
                  echo snapmap_build_single( $frontpage_id, '100%', '100vh', '15', false, $marker ); 
                }
                ?>
            </div>
            <div class="col-12 col-lg-7 section-col-6 section-form align-items-center d-flex">
                <div class="section-form-inner w-100">
                <?php
                  if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                  endwhile; endif;
                ?>
                  <div class="row contact-details">
                    <div class="col-12 col-md-6">
                      <h4>Address:</h4>
                      <p>1100 Market Street, Suite 600<br>Chattanooga, Tennessee 37402</p>
                    </div>
                    <div class="col-12 col-md-6">
                      <h4>Email:</h4>
                      <a href="mailto:hello@pixelwatt.com">hello@pixelwatt.com</a>
                    </div>
                  </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    
<?php get_footer(); ?>