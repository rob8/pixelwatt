<?php 
/* 
Template Name: City of Huntsville
*/ 
?>

<?php get_header(); ?>
    <header class='header-casestudy' id='header-cityofhsv'>
      <div class='container-fluid'>
        <div class='row'>
          <div class='col-xs-6'>
            <a class='logo-inverted' href='<?php echo site_url(); ?>' id='logo'>
              PixelWatt
            </a>
          </div>
          <div class='col-xs-6 text-right hamburger'>
            <span class='overlay-toggle' onclick='openNav()'>
              <span class='fa-stack fa-lg'>
                <i class='fa fa-circle fa-stack-2x'></i>
                <i class='fa fa-bars fa-stack-1x fa-inverse'></i>
              </span>
              <!-- /%i.fa.fa-bars{'aria-hidden' => true} -->
            </span>
          </div>
        </div>
      </div>
      <div class='header-content-container'>
        <div class='container header-content'>
          <div class='row'>
            <div class='col-xs-12'>
              <div class='row'>
                <div class='col-xs-12 col-md-8 col-md-offset-2 text-center'>
                  <h1>
                    Stepping Up the Rocket City's Digital Presence
                  </h1>
                  <p class='subtitle'>
                    Client: City of Huntsville, Alabama
                  </p>
                  <p>
                    The City of Huntsville stands in stark contrast with the rest of the state that surrounds it. While most of Alabama relies on agriculture and manufacturing, Huntsville has become one of the leading tech beds in the United States. In 2016, the city began to revamp their digital presence. Working with Fave Creative's Adriane Van Kirk to implement her approved designs, PixelWatt built a highly-customized, enterprise-grade Wordpress solution to specifically fit the city's extensive needs. PixelWatt then designed and developed a separate blog that seamlessly integrates back into the main site.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
    <hr>
    <div class='casestudy-content'>
      <div class='casestudy-services text-center'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12'>
              <h2>
                Services
              </h2>
            </div>
          </div>
          <div class='row'>
            <div class='col-xs-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdesign.png'>
                </div>
              </div>
              <h4>
                Website Design
              </h4>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-webdev.png'>
                </div>
              </div>
              <h4>
                Wordpress Development
              </h4>
            </div>
            <div class='col-xs-12 col-sm-6 col-md-4 service'>
              <div class='image-container' data-mh='service-group'>
                <div class='inner'>
                  <img src='<?php bloginfo('template_directory') ?>/images/casestudy-service-security.png'>
                </div>
              </div>
              <h4>
                Security &amp; Backups
              </h4>
            </div>
          </div>
        </div>
      </div>
      <div class='casestudy-image' id='cityofhsv-quote'>
        <div class='casestudy-caption'>
          City Blog Concept Artwork
        </div>
      </div>
      <hr>
      <div class='casestudy-website' id='cityofhsv-website'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-md-8 col-md-offset-2 text-center'>
              <h2>
                An Integrated Blog
              </h2>
              <p>
                After developing a new website for the City of Huntsville, PixelWatt designed and developed a separate blog for the city and integrated it with the main site.
              </p>
            </div>
          </div>
        </div>
        <div class='owl-carousel owl-carousel-website'>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-responsive' height='900' src='<?php bloginfo('template_directory') ?>/images/hsvblog-web-01.png' width='1400'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-responsive' height='900' src='<?php bloginfo('template_directory') ?>/images/hsvblog-web-01a.png' width='1400'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-responsive' height='900' src='<?php bloginfo('template_directory') ?>/images/hsvblog-web-02.png' width='1400'>
          </div>
          <div class='item'>
            <a class='next-website'>
              &nbsp;
            </a>
            <a class='prev-website'>
              &nbsp;
            </a>
            <img class='img-responsive' height='900' src='<?php bloginfo('template_directory') ?>/images/hsvblog-web-03.png' width='1400'>
          </div>
        </div>
      </div>
      <hr>
      <!-- #vencomatic-brochures.casestudy-singlewide -->
      <!-- .container-fluid -->
      <!-- .row -->
      <!-- .col-xs-12.text-center -->
      <!-- %h2 -->
      <!-- New Product Brochures -->
      <!-- %img.img-responsive{:src => 'images/vencomatic-brochures-product.png', :width => '1817', :height => '697'} -->
      <div class='casestudy-image' id='cityofhsv-modal'>
        <div class='casestudy-caption'>
          City Blog Author Modal
        </div>
      </div>
      <hr>
    </div>
    <div class='casestudy-contact'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-sm-10 col-sm-offset-1 text-center'>
            <h2>
              PixelWatt helped Huntsville rebuild it's digital presence.
              <strong>
                Let’s talk about what we could do for your business.
              </strong>
            </h2>
            <p>
              Fill out the form below to get the conversation started.
            </p>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            
            	<?php the_content(); ?>
            
            <?php endwhile; else: ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>